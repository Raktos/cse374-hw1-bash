#!/bin/bash

if [ $# == 0  ]
then
    echo "provide some files for conversion as arguments" >&2
else
    for arg in $@
    do
	if [ ! -e $arg  ]
	then
	    echo "$arg : file not found" >&2
	else
	    # actually do the thing, this is theoretically a valid txt
	    for word in $(head -n2 $arg)
	    do
		head+=$word
	    done

	    line=$(head -n2 $arg)
	    head=($line)
	    
	    width=${head[0]}
	    height=${head[1]}

	    widthCounter=0
	    heightCounter=0

	    echo "# ImageMagick pixel enumeration: $width,$height,255,srgb" > txtToPngTmp.txt

	    tail -n+3 $arg | while read line
	    do
		rgb=${line// /,}
		echo "$widthCounter,$heightCounter: ($rgb)" >> txtToPngTmp.txt

		# increment width and height
		((widthCounter++))
		if ((widthCounter >= width))
		then
		    ((widthCounter=0))
		    ((heightCounter++))
		fi

		# break out if there are extra line for some reason
		if ((heightCounter >= height))
		then
		    break
		fi
	    done

	    convert txt:txtToPngTmp.txt ${arg//.txt/.png}

	    rm txtToPngTmp.txt
	fi
    done
fi
