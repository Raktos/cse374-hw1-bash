#!/bin/bash

echo "#N time" > moonlight.dat
echo "#N time" > rosebud.dat

startNum=500
maxNum=7500
numInterval=500

for (( i=startNum; i <= maxNum; i+=numInterval ))
do
	timeTaken=$(/usr/bin/time -f "%U" 2>&1 ./moonlight $i > /dev/null)
	echo "$i $timeTaken" >> moonlight.dat

	timeTaken=$(/usr/bin/time -f "%U" 2>&1 ./rosebud $i > /dev/null)
	echo "$i $timeTaken" >> rosebud.dat
done

gnuplot -p -c plot.gp
